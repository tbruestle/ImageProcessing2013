f = zeros(32);
f(15:18,15:18) = ones(4);

h = zeros(32);
h(15:18,15:18) = ones(4);
h(1:4,1:4) = ones(4);
h(29:32,1:4) = ones(4);
h(1:4,29:32) = ones(4);
h(29:32,29:32) = ones(4);


conv2fh = conv2(f,h);
centerConv2fh = conv2fh(29:35,29:35)
middleTopLeftConv2fh = conv2fh(15:21,15:21)