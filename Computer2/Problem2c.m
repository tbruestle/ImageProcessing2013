fxy = zeros(64);
for x=1:64,
    for y=1:64,
       fxy(x,y) = cos(2*pi*4*x/64)*cos(2*pi*8*y/64);
    end
end

fxyimg = floor((fxy+1)/2*15.99999999999);

imwrite(mat2gray(fxyimg),'2c.tif');
subplot(1,1,1), subimage(imread('2c.tif'));