imgSize = 5; %% The size of the filter.


[f1,f2] = freqspace(imgSize,'meshgrid');
r = sqrt(f1.^2 + f2.^2); %% Set up the definition of the filter to be around a radius.
colormap(jet(64))

win = fspecial('gaussian',imgSize,2);
win = win ./ max(win(:));  

HdLowPass = ones(imgSize); 
HdLowPass(r>0.5) = 0; %% convert the images to only 1's in the radius of the circle r.
hLowPass = fwind2(HdLowPass,win); %%Turn into a gaussian filter.

HdHighPass = ones(imgSize); 
HdHighPass(r<0.5) = 0;  %% convert the images to only 1's outside the radius of the circle r.
hHighPass = fwind2(HdHighPass,win); %%Turn into a gaussian filter.

moon = imread('moon.tif'); %% Read moon image
moonLowPass = imfilter(moon, hLowPass, 'same'); %% Store moon image filtered with the low pass filter
moonHighPass = imfilter(moon, hHighPass, 'same'); %% Store moon image filtered with the high pass filter

subplot(2,2,1), subimage(moon); %% Add the original moon to compare.

subplot(2,2,2), subimage(moonLowPass); %% Show the low pass.
subplot(2,2,3), subimage(moonHighPass); %% Show the high pass.

a = 1.2;
b = .5;
subplot(2,2,4), subimage(a * moon - b * moonLowPass); %% Show the unsharp mask.