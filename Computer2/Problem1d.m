p = rand(32);

p1 = p * 0.1;
p2 = p * 0.5;
p3 = p * 1.0;
p4 = p * 2.0;
p5 = p * 3.0;
p6 = p * 4.0;
p7 = p * 5.0;
p8 = p * 6.0;

h = zeros(32);
h(15:18,15:18) = ones(4);
h(1:4,1:4) = ones(4);
h(29:32,1:4) = ones(4);
h(1:4,29:32) = ones(4);
h(29:32,29:32) = ones(4);

f = zeros(32);
f(15:18,15:18) = ones(4);

h1 = h + p1;
h2 = h + p2;
h3 = h + p3;
h4 = h + p4;
h5 = h + p5;
h6 = h + p6;
h7 = h + p7;
h8 = h + p8;

filter2hf = filter2(h,f);

filter2h1f = filter2(h1,f);
filter2h2f = filter2(h2,f);
filter2h3f = filter2(h3,f);
filter2h4f = filter2(h4,f);
filter2h5f = filter2(h5,f);
filter2h6f = filter2(h6,f);
filter2h7f = filter2(h7,f);
filter2h8f = filter2(h8,f);

imageFilter2hf = mat2gray(filter2hf);
imageFilter2h1f = mat2gray(filter2h1f);
imageFilter2h2f = mat2gray(filter2h2f);
imageFilter2h3f = mat2gray(filter2h3f);
imageFilter2h4f = mat2gray(filter2h4f);
imageFilter2h5f = mat2gray(filter2h5f);
imageFilter2h6f = mat2gray(filter2h6f);
imageFilter2h7f = mat2gray(filter2h7f);
imageFilter2h8f = mat2gray(filter2h8f);

subplot(3,3,1), subimage(imageFilter2hf);
subplot(3,3,2), subimage(imageFilter2h1f);
subplot(3,3,3), subimage(imageFilter2h2f);
subplot(3,3,4), subimage(imageFilter2h3f);
subplot(3,3,5), subimage(imageFilter2h4f);
subplot(3,3,6), subimage(imageFilter2h5f);
subplot(3,3,7), subimage(imageFilter2h6f);
subplot(3,3,8), subimage(imageFilter2h7f);
subplot(3,3,9), subimage(imageFilter2h8f);