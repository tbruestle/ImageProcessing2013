noisypeppers = imread('noisypeppers.tif');
noisypeppersMedian3 = mat2gray(medfilt2(noisypeppers, [3 3]));
noisypeppersMedian5 = mat2gray(medfilt2(noisypeppers, [5 5]));
noisypeppersMedian7 = mat2gray(medfilt2(noisypeppers, [7 7]));

subplot(1,4,1), subimage(noisypeppers);
subplot(1,4,2), subimage(noisypeppersMedian3);
subplot(1,4,3), subimage(noisypeppersMedian5);
subplot(1,4,4), subimage(noisypeppersMedian7);