fxy = zeros(64);
for x=1:64,
    for y=1:64,
       fxy(x,y) = cos(2*pi*4*x/64)*cos(2*pi*8*y/64);
    end
end

surf(fxy);
figure;
plot(fxy(16,1:64));