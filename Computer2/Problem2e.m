fxy = zeros(64);
for x=1:64,
    for y=1:64,
       fxy(x,y) = cos(2*pi*4*x/64)*cos(2*pi*8*y/64);
    end
end

fxyimg = floor((fxy+1)/2*15.99999999999);

imwrite(mat2gray(fxyimg),'2e.tif');
subplot(1,3,1), subimage(imread('2e.tif'));

fftfxy = abs(fftshift(fft2(fxy)));
minfftfxy = min(min(fftfxy));
maxfftfxy = max(max(fftfxy));


fftfxyimg = floor( (fftfxy - minfftfxy)/(maxfftfxy - minfftfxy) *15.99999999999 );

imwrite(mat2gray(fftfxyimg),'2eFFT.tif');
subplot(1,3,2), subimage(imread('2eFFT.tif'));
fftfxyInv = ifft2(ifftshift(imread('2eFFT.tif')));

minfftfxyInv = min(min(fftfxyInv));
maxfftfxyInv = max(max(fftfxyInv));

fftfxyInvImg = floor( (fftfxyInv - minfftfxyInv)/(maxfftfxyInv - minfftfxyInv) *15.99999999999 );

imwrite(mat2gray(fftfxyInvImg),'2eInv.tif');
subplot(1,3,3), subimage(imread('2eInv.tif'));
