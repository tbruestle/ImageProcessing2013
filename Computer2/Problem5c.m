imgSize = 5; %% The size of the filter.


[f1,f2] = freqspace(imgSize,'meshgrid');
r = sqrt(f1.^2 + f2.^2); %% Set up the definition of the filter to be around a radius.
colormap(jet(64))

win = fspecial('gaussian',imgSize,2);
win = win ./ max(win(:));  

HdLowPass = ones(imgSize); 
HdLowPass(r>0.5) = 0; %% convert the images to only 1's in the radius of the circle r.
hLowPass = fwind2(HdLowPass,win); %%Turn into a gaussian filter.

HdHighPass = ones(imgSize); 
HdHighPass(r<0.5) = 0;  %% convert the images to only 1's outside the radius of the circle r.
hHighPass = fwind2(HdHighPass,win); %%Turn into a gaussian filter.

a = 1.2;
b = .5;

hSharp = (a - b) * hLowPass + a * hHighPass

moon = imread('moon.tif'); %% Read moon image

imwrite(moonSharp,'5c.tif');

subplot(1,1,1), subimage(imread('5c.tif')); %% Show the unsharp mask
