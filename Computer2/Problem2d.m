fxy = zeros(64);
for x=1:64,
    for y=1:64,
       fxy(x,y) = cos(2*pi*4*x/64)*cos(2*pi*8*y/64);
    end
end

fftfxy = abs(fftshift(fft2(fxy)));
minfftfxy = min(min(fftfxy));
maxfftfxy = max(max(fftfxy));


fxyimg = floor( (fftfxy - minfftfxy)/(maxfftfxy - minfftfxy) *15.99999999999 );
imwrite(mat2gray(fxyimg),'2d.tif');
subplot(1,1,1), subimage(imread('2d.tif'));