
noisypeppers = imread('noisypeppers.tif');
noisypeppersMedian = mat2gray(medfilt2(noisypeppers, [3 3]));

subplot(1,2,1), subimage(noisypeppers);

subplot(1,2,2), subimage(noisypeppersMedian);