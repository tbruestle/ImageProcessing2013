function I1 = C3P4ProcessImage( fileName )
%C3P4PROCESSIMAGE Summary of this function goes here
%   Detailed explanation goes here


% First, I load the original image
I1 = imread(fileName);
subplot(3,3,1), subimage(I1), title(strcat(fileName , ' Original Image'));

% Second, I convert it to black and white based on a threshold.
% This is done as the formula requires distinct objects and we can't work with gray.
I1 = im2bw(I1,.2);
subplot(3,3,2), subimage(I1), title(strcat(fileName , ' Black and White Image'));

% I follow with an open, to eliminate the excess white noise.
% This open is done across a circular filter.
circleRad1 = C3P4GetCircle( 10 );
I1 = imdilate(imerode(I1,circleRad1),circleRad1);
subplot(3,3,3), subimage(I1), title(strcat(fileName , ' First Open'));

% I follow with a close, to eliminate the excess black noise.
% This close is done across a circular filter.
circleRad2 = C3P4GetCircle( 10 );
I1 = imerode(imdilate(I1,circleRad2),circleRad2);
subplot(3,3,4), subimage(I1), title(strcat(fileName , ' First Close'));

% With the noise eliminated, we can perform a larger open to smooth towards
% a circle.
% This open is done across a circular filter.
circleRad3 = C3P4GetCircle( 14 );
I1 = imdilate(imerode(I1,circleRad3),circleRad3);
subplot(3,3,5), subimage(I1), title(strcat(fileName , ' Second Open'));

% We eliminate the partial circles on the edges as they will only give
% false centers and radiuses of the circles.
I1 = imcomplement(I1);
I1 = imfill(I1,'holes') - I1;
subplot(3,3,6), subimage(I1), title(strcat(fileName , ' Remove Edge Circles'));

% For the remaining shapes, we will estimate a circle. This is done by
% performing a filling operation and finding the center of the image. For
% this, I take the average of the pixels. For the radius, I used the
% farthest right, left, top, and bottom pixels of the object.
[I1, center, radius1] = C3P4GetFirstCircle( I1 );
I1 = C3P4DrawCircles(I1, center, radius1);

% No that we have the circles, we can now find the distance between the
% centers, the average radius, and the average angle. The average radius is
% simply the average of the input radius. To find the average distance, we
% calculate the distance between every point and find the closest distance.
% To find the closest circle. For the average angle, I found the angle of
% the object at the closest distance. I then perform a mod pi/2 on the
% average angles to find the rotation of the grid.
[ angle, radius, distance ] = C3P4GetGridDetails( center, radius1 );
angleDegrees = angle * 180 / pi;
subplot(3,3,7), subimage(I1), title(strcat(fileName , ' Image Circles'));

% For the relative dot area, it is simply pi*radius^2/distance^2.
relDotArea = pi * radius.^2 / distance.^2;
disp(['Angle of ',fileName,' = ',num2str(angleDegrees),' degrees'])
disp(['Radius of ',fileName,' = ',num2str(radius),' pixels'])
disp(['Distance of ',fileName,' = ',num2str(distance),' pixels'])
disp(['Relative Dot Area of ',fileName,' = ',num2str(relDotArea)])
end

