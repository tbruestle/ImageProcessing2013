I1 = imread('tam.tif');
subplot(3,3,1), subimage(I1), title('tam.tif');
[I5,threshI5] = edge(I1,'prewitt',[],'both');
subplot(3,3,5), subimage(I5), title('Default Prewitt');
[I6,threshI6] = edge(I1,'prewitt',[],'horizontal');
subplot(3,3,6), subimage(I6), title('Horizontal Only');
[I4,threshI4] = edge(I1,'prewitt',[],'vertical');
subplot(3,3,4), subimage(I4), title('Vertical Only');
[I2,threshI2] = edge(I1,'prewitt',threshI5*0.5,'both');
subplot(3,3,2), subimage(I2), title('Lower Threshold');
[I8,threshI8] = edge(I1,'prewitt',threshI5*1.5,'both');
subplot(3,3,8), subimage(I8), title('Higher Threshold');
[I9,threshI9] = edge(I1,'prewitt',[],'both','nothinning');
subplot(3,3,9), subimage(I9), title('No Thinning');