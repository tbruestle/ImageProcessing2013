I5 = imread('tools.tif');
subplot(3,3,5), subimage(I5), title('tools.tif');

I1 = bwmorph(I5,'dilate');
subplot(3,3,1), subimage(I1), title('dilate bwmorph');
I2 = imdilate(I5,ones(3));
subplot(3,3,2), subimage(I2), title('dilate');

I3 = bwmorph(I5,'erode');
subplot(3,3,3), subimage(I3), title('erode bwmorph');
I6 = imerode(I5,ones(3));
subplot(3,3,6), subimage(I6), title('erode');

I9 = bwmorph(I5,'open');
subplot(3,3,9), subimage(I9), title('open bwmorph');
I8 = imdilate(imerode(I5,ones(3)),ones(3));
subplot(3,3,8), subimage(I8), title('open - erode then dilate');

I7 = bwmorph(I5,'close');
subplot(3,3,7), subimage(I7), title('close bwmorph');
I4 = imerode(imdilate(I5,ones(3)),ones(3));
subplot(3,3,4), subimage(I4), title('close - dilate then erode');