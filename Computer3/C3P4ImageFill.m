function [iOut, center, radius] = C3P4ImageFill( IIn, i, j )
%C3P4IMAGEFILL Summary of this function goes here
%   Detailed explanation goes here
    ITemp = IIn;
    fill = [i j];
    fill2 = zeros(0,2);
    while 0 < size(fill,1)
        fill2 = [fill2;fill(1,:)];
        [ITemp, fill] = C3P4ImageFillRecursive( ITemp, fill );
    end
    minf = min(fill2);
    maxf = max(fill2);
    center = (minf + maxf)/2;
    r1 = center(1)-minf(1);
    r2 = maxf(1)-center(1);
    r3 = center(2)-minf(2);
    r4 = maxf(2)-center(2);
    radius = max([r1 r2 r3 r4]);
    iOut = ITemp;
end

