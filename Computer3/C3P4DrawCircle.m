function IOut = C3P4DrawCircle( IIn, center, radius )
    %C3P4GETCIRCLES Draws a circle at a desired location
    %   Draws a circle at a desired location

    IOut = IIn;
    [height,width] = size(IOut);


    r = zeros(height,width);
    for i = 1:height
        for j = 1:width
          r(i,j) = sqrt((center(1) - i).^2 + (center(2) - j).^2);
          if r(i,j)<=radius
            IOut(i,j) = 1;
          end
        end
    end
end