I = imread('tools.tif');

subplot(3,3,2), subimage(I), title('tools.tif');

sobel = edge(I,'sobel');
subplot(3,3,4), subimage(sobel), title('Sobel');

prewitt = edge(I,'prewitt');
subplot(3,3,5), subimage(prewitt), title('Prewitt');

roberts = edge(I,'roberts');
subplot(3,3,6), subimage(roberts), title('Roberts');

log = edge(I,'log');
subplot(3,3,7), subimage(log), title('Log');

zerocross = edge(I,'zerocross');
subplot(3,3,8), subimage(zerocross), title('Zero Cross');

canny = edge(I,'canny');
subplot(3,3,9), subimage(canny), title('Canny');