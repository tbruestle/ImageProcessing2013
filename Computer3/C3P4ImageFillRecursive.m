function [ IOut, newFill ] = C3P4ImageFillRecursive( IIn, oldFill )
%C3P4IMAGEFILLRECURSIVE Summary of this function goes here
%   Detailed explanation goes here
    tempFill = oldFill;
    i = tempFill(1,1);
    j = tempFill(1,2);
    ITemp = IIn;
    if ITemp(i,j) == 0
        ITemp(i,j) = 1;
        if  ITemp(i+1, j) == 0
            tempFill = [tempFill;[i+1,j]];
        end
        if  ITemp(i-1, j) == 0
            tempFill = [tempFill;[i-1,j]];
        end
        if  ITemp(i, j+1) == 0
            tempFill = [tempFill;[i,j+1]];
        end
        if  ITemp(i, j-1) == 0
            tempFill = [tempFill;[i,j-1]];
        end
    end
    IOut = ITemp;
    tempFill(1,:) = [];
    newFill = tempFill;
end

