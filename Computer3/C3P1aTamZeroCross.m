I1 = imread('tam.tif');
subplot(2,3,1), subimage(I1), title('tam.tif');
[I5,threshI5] = edge(I1,'zerocross',[]);
subplot(2,3,5), subimage(I5), title('Default Zero Cross');
[I4,threshI4] = edge(I1,'zerocross',threshI5*0.5);
subplot(2,3,4), subimage(I4), title('Lower Threshold');
[I6,threshI6] = edge(I1,'zerocross',threshI5*1.5);
subplot(2,3,6), subimage(I6), title('Higher Threshold');