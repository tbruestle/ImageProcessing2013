I1 = imread('asante1.tif');
subplot(2,3,1), subimage(I1), title('asante1');
I2 = imread('asante2.tif');
subplot(2,3,2), subimage(I2), title('asante2');
I3 = imread('asante3.tif');
subplot(2,3,3), subimage(I3), title('asante3');

[I4,thresh1] = edge(I1,'sobel');
thresh1
subplot(2,3,4), subimage(I4), title('Sobel1');

[I5,thresh2] = edge(I2,'sobel');
subplot(2,3,5), subimage(I5), title('Sobel2');
thresh2

[I6,thresh3] = edge(I3,'sobel');
subplot(2,3,6), subimage(I6), title('Sobel3');
thresh3