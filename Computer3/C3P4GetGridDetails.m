function [ angle, radius, distance ] = C3P4GetGridDetails( centerIn, radiusIn )
    %C3P4GETGRIDDETAILS Summary of this function goes here
    %   Detailed explanation goes here

    centerCount = size(centerIn, 1);
    distances = zeros(centerCount, centerCount);
    angles = zeros(centerCount, centerCount);
    mindistances = zeros(centerCount, 3);
    for i = 1:centerCount
        for j = 1:centerCount
            if i ~= j
                yDiff = (centerIn(i,1)-centerIn(j,1));
                xDiff = (centerIn(i,2)-centerIn(j,2));
                distances(i,j) = sqrt(xDiff.^2 + yDiff.^2);

                if xDiff == 0
                    if (yDiff > 0)
                        angles(i,j) = pi/2;
                    else
                        angles(i,j) = 3*pi/2;
                    end
                elseif yDiff == 0
                    if (xDiff > 0)
                        angles(i,j) = 0;
                    else
                        angles(i,j) = pi;
                    end
                else
                    if (yDiff > 0)
                        angles(i,j) = atan(yDiff/xDiff);
                    else
                        angles(i,j) = pi + atan(yDiff/xDiff);
                    end
                end

                if j==1 || (i==1 && j==2)
                    mindistances(i,1) = j;
                    mindistances(i,2) = distances(i,j);
                    mindistances(i,3) = angles(i,j);
                elseif distances(i,j) < mindistances(i,2)
                    mindistances(i,1) = j;
                    mindistances(i,2) = distances(i,j);
                    mindistances(i,3) = angles(i,j);
                end
            end
        end
    end
    
    distance = mean(mindistances(:,2));
    angle = mean(mod(mindistances(:,3),pi/2));
    radius = mean(radiusIn);
end

