function IOut = C3P4DrawCircles( IIn, center, radius )
    %C3P4GETCIRCLES Draws circles at multiple desired locations
    %   Draws circles at multiple desired locations
    IOut = IIn;
    centers = size(center,1);
    for i = 1:centers
        IOut = C3P4DrawCircle(IOut,center(i,:),radius(i,:));
    end
end