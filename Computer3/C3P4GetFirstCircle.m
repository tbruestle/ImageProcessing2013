function [iOut, center, radius] = C3P4GetFirstCircle( I1 )
    %C3P4GETFIRSTCIRCLE Summary of this function goes here
    %   Detailed explanation goes here
    ITemp = I1;
    [height, width] = size(ITemp);

    ITemp = imcomplement(ITemp);

    
    center = zeros(2,0);
    radius = zeros(1,0);
    count = 0;
    for i = 1:1:height
        for j = 1:1:width
            if ITemp(i,j) == 0
                [ITemp, centerNew, radiusNew] = C3P4ImageFill(ITemp, i, j);
                if count == 0
                    center = centerNew;
                    radius = radiusNew;
                else
                    center = [center;centerNew];
                    radius = [radius;radiusNew];
                end
                count = count + 1;
            end
        end
    end
    iOut = imcomplement(ITemp);
end

