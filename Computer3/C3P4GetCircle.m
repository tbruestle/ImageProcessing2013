function circleRad1 = C3P4GetCircle( Rad1 )
%C3P4GETCIRCLES Summary of this function goes here
%   Detailed explanation goes here


center = Rad1  + 1;

 
 
circleRad1 = zeros(Rad1*2-1);
r = zeros(center*2 - 1);
for i = 1:center*2 - 1
    for j = 1:center*2 - 1
      r(i,j) = sqrt((center - i).^2 + (center - j).^2);
      if r(i,j)<=Rad1
        circleRad1(i,j) = 1;
      end
    end
end



end

