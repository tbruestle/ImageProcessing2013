function [ circleRad1,circleRad2,circleRad3 ] = C3P4GetCircles( Rad1, Rad2, Rad3 )
%C3P4GETCIRCLES Summary of this function goes here
%   Detailed explanation goes here


center = max([Rad1, Rad2, Rad3])  + 1;

circleRad1 = zeros(Rad1*2-1);
circleRad2 = zeros(Rad2*2-1);
circleRad3 = zeros(Rad3*2-1);
 
r = zeros(center*2 - 1);
 
 
for i = 1:center*2 - 1
    for j = 1:center*2 - 1
      r(i,j) = sqrt((center - i).^2 + (center - j).^2);
      if r(i,j)<=Rad1
        circleRad1(i,j) = 1;
      end
      if r(i,j)<=Rad2
        circleRad2(i,j) = 1;
      end
      if r(i,j)<=Rad3
        circleRad3(i,j) = 1;
      end
    end
end



end

