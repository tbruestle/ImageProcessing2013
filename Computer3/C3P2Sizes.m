I5 = imread('tools.tif');
subplot(3,3,5), subimage(I5), title('tools.tif');

I1 = imdilate(I5,ones(3));
subplot(3,3,1), subimage(I1), title('dilate 3x3');
I4 = imdilate(I5,ones(5));
subplot(3,3,4), subimage(I4), title('dilate 5x5');
I7 = imdilate(I5,ones(7));
subplot(3,3,7), subimage(I7), title('dilate 7x7');

I3 = imerode(I5,ones(3));
subplot(3,3,3), subimage(I3), title('erode 3x3');
I6 = imerode(I5,ones(5));
subplot(3,3,6), subimage(I6), title('erode 5x5');
I9 = imerode(I5,ones(7));
subplot(3,3,9), subimage(I9), title('erode 7x7');