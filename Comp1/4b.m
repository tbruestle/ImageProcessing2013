a=ones(32,1);
b=zeros(32,1);
c=[a b];
d=[c c c c c c c c c c c c c c c c];
d=logical(uint8(d));
e=[d d;d d];
f=[e e;e e];

fBicubic = imresize(f,0.5,'bicubic'); 
fBilinear = imresize(f,0.5,'bilinear'); 
fNearest = imresize(f,0.5,'nearest'); 

subplot(1,3,1), subimage(fBicubic);
subplot(1,3,2), subimage(fBilinear);
subplot(1,3,3), subimage(fNearest);