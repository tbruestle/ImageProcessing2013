a=ones(32,1);
b=zeros(32,1);
c=[a b];
d=[c c c c c c c c c c c c c c c c];,'nearest'
d=logical(uint8(d));
e=[d d;d d];
f=[e e;e e];

f1 = imrotate(f,45); 

fBicubic = imresize(f1,0.5,'bicubic'); 
fBilinear = imresize(f1,0.5,'bilinear','Antialiasing',1); 
fBilinearNoAntialias = imresize(f1,0.5,'bilinear','Antialiasing',0); 
fNearest = imresize(f1,0.5,'nearest'); 

subplot(2,3,1), subimage(fBicubic);
subplot(2,3,2), subimage(fBilinear);
subplot(2,3,3), subimage(fBilinearNoAntialias);
subplot(2,3,4), subimage(fNearest);
subplot(2,3,5), subimage(f1);