A = imread('Comp1Images/mri.tif');
B1 = imrotate(A,45,'bilinear');
B2 = imrotate(A,45,'nearest');
%Resize for better visibility as zoom has limits
B1crop = imresize(imcrop(B1,[60 65 4 4]),50,'nearest'); 
B2crop = imresize(imcrop(B2,[60 65 4 4]),50,'nearest');

subplot(1,2,1), subimage(B1crop)
subplot(1,2,2), subimage(B2crop)