A = imread('Comp1Images/rice.png');
B = imhist(A);
plot(B);
[m,n] = size(B);

lowGray = -1;
highGray = -1;

for i = 0:m-1
    if lowGray < 0 &&  B(i+1) > 0
        lowGray = i
        lowGrayVal = B(i+1)
    end
end
for i = m-1:-1:0
    if highGray < 0 &&  B(i+1) > 0
        highGray = i
        highGrayVal = B(i+1)
    end
end