x = [0 0.25 0.5 0.75 1 0.5 0.5];
y = [0.5 0.375 0.25 0.125 0 0.5 0.75];
lenx = length(x);
theta = zeros(lenx,lenx);
ro = zeros(lenx,lenx);

for i = 1:1:lenx
    for j = 1:1:lenx
        if i~=j
            if y(i) == y(j)
                theta(i,j) = pi/2;
            else
                theta(i,j) = mod(atan(-(x(i) - x(j))/(y(i) - y(j))),pi);
            end
            ro(i,j) = x(i)*cos(theta(i,j)) + y(i)*sin(theta(i,j));
        end
    end    
end
theta
ro