A = int16([...
    0 3 2 5 5 5; ...
    0 3 5 5 5 5; ...
    0 3 5 7 7 7; ...
    3 3 5 7 7 7; ...
    3 4 7 7 7 7; ...
    4 7 7 7 7 4 ...
    ]);

x = [0 1 2 3 4 5 6 7];
y = zeros(1,8);

for i = 1:6
    for j = 1:6
        for k = 1:8
            if A(i,j) + 1 == k
                y(k) = y(k) + 1/36;
            end
        end
    end
end
H1 = zeros(1,8);
H = 0;
for k = 1:8
    if y(k) == 0
        H1(k) = 0;
    else
        H1(k) = y(k) * -log2(y(k));
    end
    H = H + H1(k);
end
H